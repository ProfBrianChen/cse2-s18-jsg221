//Jonathan Guzman_2/2/18_CSE02_Section 060
// In this program, this code will print the number of minutes/counts for each trip.
//It will also print out the distance of each trip and 2 trips combined.
public class Cyclometer
{
  public static void main (String[] args)
  {
    int secsTrip1=480; //This line is declaring secsTrip1 as an int and given the value 480
    int secsTrip2=3220; //This line is declaring secsTrip2 as an int and given the value 3220
    int countsTrip1=1561; //This line is declaring countsTrip1 as an int and giving it the value 1561
    int countsTrip2=9037; //This line is declaring countsTrip2 as an int and giving the value 9037
    double wheelDiameter= 27.0, //This line is declaring wheelDiameter as a double with the value 27.0
    PI = 3.14159, //This line is declaring PI as 3.14159
    feetPerMile=5280,
    inchesperFoot=12,
    secondsPerMinute=60;
    double distanceTrip1, distanceTrip2, totalDistance;
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+ " minutes and had " +
                      countsTrip1+ "counts.");
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+
                      countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    distanceTrip1/= inchesperFoot*feetPerMile;
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesperFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
  }
}