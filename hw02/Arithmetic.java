public class Arithmetic
{
  public static void main (String[] args)
  {
    int numPants = 3;
    double pantsPrice = 34.98;
    
    int numShirts = 2;
    double shirtPrice = 24.99;
    
    int numBelts = 1;
    double beltCost = 39.99;
    
    double paSalesTax = 0.06;
    
    double totalCostOfPants = pantsPrice*numPants;
    double totalCostOfShirts = numShirts*shirtPrice;
    double totalCostOfBelts = numBelts*beltCost;
    
    double taxOnPants = totalCostOfPants*paSalesTax;
    double taxOnShirts = totalCostOfShirts*paSalesTax;
    double taxOnBelts = totalCostOfBelts*paSalesTax;
    
    double totalCostOfPantsWithTax = totalCostOfPants + taxOnPants;
    double totalCostOfShirtsWithTax = totalCostOfShirts + taxOnShirts;
    double totalCostOfBeltsWithTax = totalCostOfBelts + taxOnBelts;
    
    double totalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    double totalTax = taxOnPants+taxOnBelts+taxOnBelts;
    double totalCostWithTax = totalCostOfPantsWithTax+totalCostOfShirtsWithTax+totalCostOfBeltsWithTax;
    
    System.out.println("Pants: $"+totalCostOfPantsWithTax);
    System.out.println("Shirts: $"+totalCostOfShirtsWithTax);
    System.out.println("Belts: $"+totalCostOfBeltsWithTax);
    System.out.println("Total Cost: $"+ totalCostWithTax);
     
  }
}